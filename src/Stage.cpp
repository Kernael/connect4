#include "Stage.h"
#include "Globals.h"
#include <iostream>

Stage::Stage()
	: _grid(), _entityManager(), _position(0, 0), _turn(true), _hasEntity(false)
{
	Globals::factory.createObject("piece");
}

Stage::~Stage()
{
}

bool Stage::hasEntity() const
{
	return _hasEntity;
}

Stage::State Stage::processEvents(const sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Return:
			this->createPiece(_position.first);
			break;
		case sf::Keyboard::Up:
			this->setPosition(_position.first + 1, _position.second);
			break;
		case sf::Keyboard::Right:
			this->setPosition(_position.first + 1, _position.second);
			break;
		case sf::Keyboard::Left:
			this->setPosition(_position.first - 1, _position.second);
			break;
		case sf::Keyboard::Down:
			this->setPosition(_position.first - 1, _position.second);
			break;
		default:
			break;
		}
	default:
		break;
	}

	return State::Idle;
}

Stage::State Stage::update(float)
{
	for (int entity = 0; entity < EntityManager::ENTITY_COUNT; ++entity)
	{
		_entityManager.updateEntity(entity, _grid);
	}

	return State::Idle;
}

void Stage::draw()
{
	if (!this->hasEntity())
	{
		return;
	}

	sf::CircleShape	shape;
	Color			color = { 255, 255, 255 };

	for (int entity = 0; entity < EntityManager::ENTITY_COUNT; ++entity)
	{
		if (_entityManager.hasComponent(entity, RENDER_MASK))
		{
			color = _entityManager.getColor(entity);

			shape = Globals::factory.getObject(_entityManager.getAppearance(entity));
			shape.setFillColor(sf::Color(color.r, color.g, color.b));

			std::pair<float, float> coord = _grid.coordinatesToPixels(
				_entityManager.getDisplacement(entity).x,
				_entityManager.getDisplacement(entity).y
				);

			shape.setPosition(coord.first, coord.second);

			Globals::window->draw(shape);
		}
	}
}

void Stage::createPiece(int x)
{
	if (_grid.validPosition(x, _grid.getRows() - 1))
	{
		_hasEntity = true;

		int piece = _entityManager.createEntity(
			EComponent::APPEARANCE |
			EComponent::COLLIDABLE |
			EComponent::VELOCITY |
			EComponent::GRAVITY |
			EComponent::DISPLACEMENT |
			EComponent::COLORED
			);

		_entityManager.setDisplacement(piece, std::make_pair(x, _grid.getRows() - 1));
		_entityManager.setAppearance(piece, "piece");

		_grid.fillCell(x, _grid.getRows() - 1, 1);

		if (_turn)
		{
			_entityManager.setColor(piece, Color{ 255, 255, 0 });
			_turn = false;
		}
		else
		{
			_entityManager.setColor(piece, Color{ 255, 0, 0 });
			_turn = true;
		}
	}
}

void Stage::setPosition(int x, int y)
{
	if (x < _grid.getColumns() && x >= 0)
	{
		Globals::logger << "Moved from " + std::to_string(_position.first)
			+ " to " + std::to_string(x) << std::endl;
		_position.first = x;
	}

	if (y < _grid.getRows() && y >= 0)
	{
		_position.second = y;
	}
}