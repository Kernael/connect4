#include "Grid.h"
#include "Globals.h"

Grid::Grid(int cols, int rows)
	: _cellSize(), _grid()
{
	_columns = cols;
	_rows = rows;

	for (int y = 0; y < _rows; ++y)
	{
		for (int x = 0; x < _columns; ++x)
		{
			_grid.insert(
				std::make_pair(std::make_pair(x, y), 0
				));
		}
	}

	_cellSize.first = static_cast<float>(Globals::window->getSize().x) /
		static_cast<float>(_columns);
	_cellSize.second = static_cast<float>(Globals::window->getSize().y) /
		static_cast<float>(_rows);
}

Grid::~Grid()
{
}

int Grid::getColumns() const
{
	return _columns;
}

int Grid::getRows() const
{
	return _rows;
}

const std::pair<float, float>& Grid::getCellSize() const
{
	return _cellSize;
}

const Grid::GridMap& Grid::getGrid() const
{
	return _grid;
}

bool Grid::validPosition(int x, int y) const
{
	if (x < 0 || y < 0)
	{
		return false;
	}

	if (_grid.at(std::make_pair(x, y)))
	{
		return false;
	}

	return true;
}

std::pair<float, float> Grid::coordinatesToPixels(int x, int y) const
{
	return std::make_pair(x * _cellSize.first, (_rows - 1 - y) * _cellSize.second);
}

void Grid::fillCell(int x, int y, int marker)
{
	_grid.at(std::make_pair(x, y)) = marker;
}

void Grid::cleanCell(int x, int y)
{
	_grid.at(std::make_pair(x, y)) = 0;
}