#include "ObjectFactory.h"

ObjectFactory::ObjectFactory() : _objects()//, _textures()
{
}

ObjectFactory::~ObjectFactory()
{
}

const sf::CircleShape& ObjectFactory::getObject(const std::string& name) const
{
	return _objects.at(name);
}

sf::CircleShape& ObjectFactory::object(const std::string& name)
{
	return _objects.at(name);
}

sf::CircleShape& ObjectFactory::createObject(const std::string& name)
{
	if (_objects.find(name) == _objects.end())
	{
		_objects.insert(std::make_pair(name, sf::CircleShape(30)));
	}

	return _objects.at(name);
}