#include "Globals.h"

std::unique_ptr<sf::RenderWindow>	Globals::window = std::unique_ptr<sf::RenderWindow>(
	new sf::RenderWindow(sf::VideoMode(800, 600), "Connect4"
	));
ObjectFactory	Globals::factory = ObjectFactory();
Logger			Globals::logger = Logger("../connect4.log");