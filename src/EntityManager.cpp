#include "EntityManager.h"
#include <Globals.h>

EntityManager::EntityManager()
	: _entities(), _displacements(), _velocities(), _appearences()
{
	_entities.fill(EComponent::NONE);
}

EntityManager::~EntityManager()
{
}

const Displacement& EntityManager::getDisplacement(int entity) const
{
	return _displacements.at(entity);
}

const Velocity& EntityManager::getVelocity(int entity) const
{
	return _velocities.at(entity);
}

const std::string& EntityManager::getAppearance(int entity) const
{
	return _appearences.at(entity);
}

const Color& EntityManager::getColor(int entity) const
{
	return _colors.at(entity);
}

void EntityManager::setDisplacement(int entity, const std::pair<int, int>& pos)
{
	if (hasComponent(entity, EComponent::DISPLACEMENT))
	{
		_displacements.at(entity) = { pos.first, pos.second };
	}
}

void EntityManager::setAppearance(int entity, const std::string& name)
{
	if (hasComponent(entity, EComponent::APPEARANCE))
	{
		_appearences.at(entity) = { name };
	}
}

void EntityManager::setColor(int entity, const Color& color)
{
	if (hasComponent(entity, EComponent::COLORED))
	{
		_colors.at(entity) = color;
	}
}

int EntityManager::createEntity(int mask)
{
	for (int entity = 0; entity < ENTITY_COUNT; ++entity)
	{
		if (_entities.at(entity) == EComponent::NONE)
		{
			_entities.at(entity) = mask;

			this->resetEntity(entity);

			Globals::logger << "Created new entity : " + std::to_string(entity)
				+ " -> " + std::to_string(mask) << std::endl;

			return entity;
		}
	}

	return ENTITY_COUNT;
}

void EntityManager::destroyEntity(int entity)
{
	if (entity < ENTITY_COUNT)
	{
		Globals::logger << "Destroyed entity : " + std::to_string(entity)
			+ " -> " + std::to_string(_entities.at(entity))
			<< std::endl;

		_entities.at(entity) = EComponent::NONE;

		this->resetEntity(entity);
	}
}

void EntityManager::resetEntity(int entity)
{
	_displacements.at(entity) = { 0, 0 };
	_velocities.at(entity) = { 1, 1 };
	_appearences.at(entity) = { "None" };
	_colors.at(entity) = { 255, 255, 255 };
}

void EntityManager::updateEntity(int entity, Grid& grid)
{
	if (hasComponent(entity, EComponent::GRAVITY) && grid.validPosition(
		_displacements.at(entity).x, _displacements.at(entity).y - _velocities.at(
		entity).y))
	{
		grid.cleanCell(_displacements.at(entity).x, _displacements.at(entity).y);
		_displacements.at(entity).y -= _velocities.at(entity).y;
		grid.fillCell(_displacements.at(entity).x, _displacements.at(entity).y, 1);

		Globals::logger << "Gravity event : " + std::to_string(entity)
			+ " -> [" + std::to_string(_displacements.at(entity).x)
			+ ", " + std::to_string(_displacements.at(entity).y)
			+ "]" << std::endl;
	}
}

bool EntityManager::hasComponent(int entity, int component) const
{
	if ((_entities.at(entity) & component) == component)
	{
		return true;
	}

	return false;
}

bool EntityManager::hasAnyComponent(int entity) const
{
	if (_entities.at(entity))
	{
		return true;
	}

	return false;
}