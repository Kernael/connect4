#include <iostream>
#include <SFML/Graphics.hpp>
#include "Stage.h"
#include "Globals.h"

int		main()
{
	try
	{
		Globals::window->setFramerateLimit(60);

		Stage		game;
		sf::Clock	clock;

		while (Globals::window->isOpen())
		{
			float		elapsedTime = clock.restart().asSeconds();

			std::cout << 1.f / elapsedTime << std::endl;

			sf::Event	event;

			while (Globals::window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed ||
					sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				{
					Globals::logger << "Window closed\n";
					Globals::window->close();
				}
				else
				{
					game.processEvents(event);
				}
			}

			Globals::window->clear();

			game.update(elapsedTime);
			game.draw();

			Globals::window->display();
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "Catched exception : " << e.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cerr << "Catched unknown exception" << std::endl;
		return 1;
	}

	return 0;
}