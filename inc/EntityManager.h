#ifndef __ENTITY_MANAGER_H__
# define __ENTITY_MANAGER_H__

# include <array>
# include "Component.h"
# include "Grid.h"

class EntityManager
{
public:
	static const int	ENTITY_COUNT = 100;

	EntityManager();
	~EntityManager();

	const Displacement& getDisplacement(int) const;
	const Velocity&		getVelocity(int) const;
	const std::string&	getAppearance(int) const;
	const Color&		getColor(int) const;

	void	setDisplacement(int, const std::pair<int, int>&);
	void	setAppearance(int, const std::string&);
	void	setColor(int, const Color&);

	int		createEntity(int);
	void	destroyEntity(int);
	void	resetEntity(int);
	void	updateEntity(int, Grid&);

	bool	hasComponent(int, int) const;
	bool	hasAnyComponent(int) const;

private:
	std::array<int, ENTITY_COUNT>			_entities;

	std::array<Displacement, ENTITY_COUNT>	_displacements;
	std::array<Velocity, ENTITY_COUNT>		_velocities;
	std::array<std::string, ENTITY_COUNT>	_appearences;
	std::array<Color, ENTITY_COUNT>			_colors;
};

#endif // !__ENTITY_MANAGER_H__
