#ifndef __GLOBALS_H__
# define __GLOBALS_H__

# include "Logger.hpp"
# include <SFML/Graphics/RenderWindow.hpp>
# include <memory>
# include "ObjectFactory.h"

namespace Globals
{
	extern std::unique_ptr<sf::RenderWindow>	window;
	extern ObjectFactory	factory;
	extern Logger			logger;
}

#endif // !__GLOBALS_H__