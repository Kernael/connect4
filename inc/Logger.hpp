#ifndef __LOGGER_HPP__
# define __LOGGER_HPP__

# include <string>
# include <fstream>
# include <ctime>

class Logger
{
public:
	Logger() : Logger("") {}
	explicit Logger(const std::string& path) : _path(path), _file()
	{
		if (!_path.empty())
		{
			_file.open(path);
			this->write("Logger created\n");
		}
	}
	virtual ~Logger()
	{
		if (_file.is_open())
		{
			this->write("Logger destroyed\n");
		}
	}

	template<typename T>
	Logger&	write(const T& stuff)
	{
		if (_file.is_open())
		{
			time_t		rawTime = time(nullptr);
			struct tm	timeInfo;

			localtime_s(&timeInfo, &rawTime);

			_file << "[" << timeInfo.tm_hour << ":" << timeInfo.tm_min << ":"
				<< timeInfo.tm_sec << "] " << stuff;
		}

		return *this;
	}
	Logger&	write(std::ostream& (*pf) (std::ostream&))
	{
		if (_file.is_open())
		{
			_file << pf;
		}

		return *this;
	}

private:
	std::string		_path;
	std::ofstream	_file;
};

template<typename T>
Logger&	operator<<(Logger& logger, const T& stuff)
{
	return logger.write(stuff);
}

inline Logger& operator<<(Logger& logger, std::ostream& (*pf) (std::ostream&))
{
	return logger.write(pf);
}

#endif // !__LOGGER_HPP__