#ifndef __STAGE_H__
# define __STAGE_H__

# include <SFML/Graphics.hpp>
# include "EntityManager.h"

class Stage
{
public:
	enum class State
	{
		Idle, Stop, Next, Prev
	};

	static const int	RENDER_MASK = (
		EComponent::APPEARANCE | EComponent::DISPLACEMENT
		);

	Stage();
	virtual ~Stage();

	virtual bool	hasEntity() const;

	virtual State	processEvents(const sf::Event&);
	virtual State	update(float);
	virtual void	draw();

	virtual void	createPiece(int);
	virtual void	setPosition(int, int);

private:
	Grid			_grid;
	EntityManager	_entityManager;

	std::pair<int, int>	_position;

	bool	_turn;
	bool	_hasEntity;
};

#endif // !__STAGE_H__
