#ifndef __COMPONENT_H__
# define __COMPONENT_H__

# include <string>

namespace EComponent
{
	enum
	{
		NONE = 0,
		DISPLACEMENT = 1 << 0,
		VELOCITY = 1 << 1,
		APPEARANCE = 1 << 2,
		GRAVITY = 1 << 3,
		COLLIDABLE = 1 << 4,
		COLORED = 1 << 5
	};
};

struct Displacement
{
	int		x, y;
};

struct Velocity
{
	int		x, y;
};

struct Appearance
{
	std::string	name;
};

struct Color
{
	int		r, g, b;
};

#endif // !__COMPONENT_H__
