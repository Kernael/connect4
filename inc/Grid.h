#ifndef __GRID_H__
# define __GRID_H__

# include <map>

class Grid
{
public:
	typedef std::map<std::pair<int, int>, int>	GridMap;

	explicit Grid(int = 7, int = 6);
	virtual ~Grid();

	int	getColumns() const;
	int	getRows() const;
	const std::pair<float, float>&	getCellSize() const;
	const GridMap&	getGrid() const;

	bool	validPosition(int, int) const;
	std::pair<float, float>	coordinatesToPixels(int, int) const;

	void	fillCell(int, int, int);
	void	cleanCell(int, int);

private:
	int		_columns, _rows;
	std::pair<float, float>	_cellSize;
	GridMap	_grid;
};

#endif // !__GRID_H__
