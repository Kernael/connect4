#ifndef __OBJECT_FACTORY_H__
# define __OBJECT_FACTORY_H__

# include <map>
# include <string>
# include "SFML/Graphics.hpp"
# include <memory>

class ObjectFactory
{
public:
	ObjectFactory();
	~ObjectFactory();

	const sf::CircleShape&	getObject(const std::string&) const;
	sf::CircleShape&		object(const std::string&);

	sf::CircleShape&	createObject(const std::string&);
	//sf::Shader&		createObject(const std::string&);

private:
	std::map<std::string, sf::CircleShape>	_objects;
	//std::map<std::string, sf::Shader>		_objects;
	//std::map<std::string, sf::Texture>	_textures;
};

#endif // !__OBJECT_FACTORY_H__
